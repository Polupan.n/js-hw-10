"use strickt";

// Теоретичні питання
// 1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?

// document.createElement(tag) - Створює новий вузол елемента з вказаним тегом
let div = document.createElement(`div`);
// document.createTextNode(text) - Створює новий вузол тексту із заданим текстом
let textNode = document.createTextNode(`Hello world!`);

// 2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.

// Спочатку потрібно отримати посилання на елемент:
const navElem = document.querySelector(`navigation`);
// Після отримання посилання на елемент викликаємо метод remove() на цьому елементі. Це призведе до видалення елементу з документа.
// navElem.remove();

// 3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
// існує спеціальний метод append:
document.body.append(div);

// Ось ще декілька методів вставки, які вказують різні місця для вставки:
// ● node.append(...nodes або strings) – додає вузли або рядки в кінець вузла,
// ● node.prepend(...nodes або strings) – вставляє вузли або рядки в початок
// вузла,
// ● node.before(...nodes або strings) – вставляє вузли або рядки перед
// вузлом,
// ● node.after(...nodes або strings) – вставляє вузли або рядки після вузла,
// ● node.replaceWith(...nodes або strings) – замінює вузол заданими вузлами
// або рядками.

// Для додавання елементу ми можемо використовувати ще один досить
// універсальний метод: elem.insertAdjacentHTML(where, html).
// Перший параметр - це кодове слово, яке вказує, де вставити відносно
// elem. Має бути одним із наступних:
// ● "beforebegin" – вставити html безпосередньо перед elem,
// ● "afterbegin" – вставити html в elem, на початку,
// ● "beforeend" – вставити html в elem, в кінці,
// ● "afterend" – вставити html безпосередньо після elem

// Практичні завдання
//  1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
let linkA = document.createElement(`a`);
linkA.textContent = `Learn More`;
linkA.setAttribute(`href`, `#`);
const footer = document.querySelector(`footer`);
const paragraph = footer.querySelector(`p`);
paragraph.insertAdjacentElement(`afterend`, linkA);
console.log(linkA);

//  2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
const select = document.createElement("select");
select.setAttribute(`id`, `rating`);
const main = document.querySelector(`main`);
main.insertAdjacentElement(`afterbegin`, select);

//  Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
// const option4 = document.createElement(`option`);
// option4.value = `4`;
// option4.textContent = `4 Stars`;
// select.appendChild(option4);

// const element = array[index];

// option.value = `${i}`;
// let star = `star`;
// option.textContent = `${i} ${star}`;
// for (let i = 0; i < ; i++) {

// }

for (let i = 1; i <= 4; i++) {
  const option = document.createElement(`option`);
  option.value = i;
  option.textContent = `${i} Stars`;
  if (i === 1) {
    option.textContent = `${i} Star`;
  }
  select.append(option);
}

//  Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
//  Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.
